package edu.uga.eits.portal.portlet.myPlacePortlet.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.IOUtils;

/**
 * the basic model adpater to get the json file from http
 * @author ucam10a
 *
 */
public abstract class JSONAdapter {
	
	
	/**
	 * the time span to reload the json file
	 */
	public static int timeSpan = 0;
	
	/**
	 * get the json file
	 * @param jsonurl json url
	 * @return json object
	 * @throws IOException
	 * @throws JSONException
	 */
	public static String getJSONObjectFromUrl(String jsonurl) throws IOException {
		
		URL url = new URL(jsonurl);
		URLConnection con = url.openConnection();
		InputStream in = con.getInputStream();
		String encoding = con.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;
		String json = IOUtils.toString(in, encoding);
		return json;
	
	}
	
}
