package edu.uga.eits.portal.portlet.myPlacePortlet.adapter;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import edu.uga.eits.portal.portlet.myPlacePortlet.model.Config;
import edu.uga.eits.portal.portlet.myPlacePortlet.model.POI;
import edu.uga.eits.portal.portlet.myPlacePortlet.model.Place;
import edu.uga.eits.portal.portlet.myPlacePortlet.model.WeekOpenHour;


/**
 * @author ucam10a
 * 
 * The adapter load the json from http server, and parse to the Map object
 * It is implemented by Register of Singleton. 
 * Only one object will be retained, if timestamp is over 15 minutes then it will reload again.
 * 
 */
public class PlaceAdapter extends JSONAdapter {
	
	/**
	 * time stamp
	 */
	private static long timestamp = 0L;
	
	/**
	 * the cache data for places
	 */
	private static HashMap<String, Place> places = new HashMap<String, Place>();
	
	/**
	 * log class
	 */
	private static Log log = LogFactory.getLog(PlaceAdapter.class);
	
	/**
	 * the url to get the json which contains all my place information
	 */
	private static String jsonurl = "";
	
	private PlaceAdapter() {
		// not implemented
	}
	
	/**
	 * get place cache
	 * @return
	 * @throws Exception
	 */
	public static HashMap<String, Place> getPlaceCache() throws Exception{
		
		// get current time
        java.util.Date date = new java.util.Date();
		
        // get class loader path
        String jsonFilePath = Config.getBaseUrl() + "mapAll.json";
                
        // config the json url from property file
        if (jsonurl == null || jsonurl.equals("")){
        	try {
    			
        		//load a json file path
        		URL jsonURL;
        		jsonURL = new URL(jsonFilePath);
    			jsonurl = jsonURL.toString();
        		System.out.println(jsonurl);
        		
        	} catch (IOException ex) {
        		ex.printStackTrace();
        		log.debug("read property file error : " + ex.toString());
        		System.out.println(ex.toString());
            }
        }
        
				
		if (((date.getTime() - timestamp) / 1000) > timeSpan || timestamp == 0){
		
			// lapse time is over 15 minutes, reload the data from http server 
			places = new HashMap<String, Place>();
			Gson gson = new Gson();
			String json = getJSONObjectFromUrl(jsonurl);
			JsonObject root = new JsonParser().parse(json).getAsJsonObject();
			JsonObject mapData = root.get("mapData").getAsJsonObject();
			JsonArray placeArr = mapData.get("locations").getAsJsonArray();
			ArrayList<Place> placeList = new ArrayList<Place>();
			for (int i = 0; i < placeArr.size(); i++){
				Place p = gson.fromJson(placeArr.get(i), Place.class);
				placeList.add(p);
			}
			System.out.println("placeList size = " + placeList.size());
			for (Place place : placeList) {
				if (place.isDisplaymyPlace()) places.put(place.getId() + "", place);
			}
			timestamp = date.getTime();
			return places;
					
		} else {
			
			// the lapse time is not over 15 minutes, use the cache data to save time for communication
			//System.out.println("lapse = "+ ((date.getTime() - timestamp) / 1000));
        	return places;
		
		}
				
	}
	
	/**
	 * test the method
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		Calendar rightNow = Calendar.getInstance();
		
		HashMap<String, Place> places = PlaceAdapter.getPlaceCache();
		for (Place p : places.values()){
			System.out.println("    " + p.getId() + ", " + p.getName() + ", " + p.getLatitude() + ", " + p.getLongitude());
		}
		
		HashMap<String, WeekOpenHour> placesHours = WeekHourAdapter.getWeekHourCache();
		for (WeekOpenHour hr : placesHours.values()){
			System.out.println("    Fri=" + hr.getFri() + ", Thr=" + hr.getThr() + ", Wed" + hr.getWed() + ", Tue=" + hr.getTue() + ", Mon=" + hr.getMon());
		}
		
		ArrayList<POI> mypois = new ArrayList<POI>();
		for (String placeId : placesHours.keySet()){
			mypois.add(new POI(places.get(placeId), placesHours.get(placeId).ConvertToWeekBlock()));
    	}
		
		for (POI p : mypois){
			System.out.println("place name = " + p.getPlace().getName());
			System.out.println("    Today's Hours = " + POI.getSchedule(rightNow, placesHours.get(p.getPlace().getId() + "").ConvertToWeekBlock()));
		}
		
	}
	
}
