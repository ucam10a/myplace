package edu.uga.eits.portal.portlet.myPlacePortlet.adapter;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import edu.uga.eits.portal.portlet.myPlacePortlet.model.Config;
import edu.uga.eits.portal.portlet.myPlacePortlet.model.POI;
import edu.uga.eits.portal.portlet.myPlacePortlet.model.Place;
import edu.uga.eits.portal.portlet.myPlacePortlet.model.WeekOpenHour;

/**
 * @author ucam10a
 * The adapter load the json from http server, and parse to the WeekOpenHour object
 * It is implemented by Register of Singleton. 
 * Only one object will be retained, if timestamp is over 15 minutes then it will reload again.
 */
public class WeekHourAdapter extends JSONAdapter {

	
	/**
	 * time stamp
	 */
	private static long timestamp = 0L;
	
	/**
	 * the cache data for place open hour
	 */
	private static HashMap<String, WeekOpenHour> placesHours = new HashMap<String, WeekOpenHour>();
	
	/**
	 * log class
	 */
	private static Log log = LogFactory.getLog(WeekHourAdapter.class);
	
	/**
	 * the url to get the json which contains all my place information
	 */
	private static String jsonurl = "";
	
	private WeekHourAdapter() {
		// not implemented
	}
	
	/**
	 * key is placeId, value is week open hour
	 * @return hash map
	 * @throws Exception
	 */
	public static HashMap<String, WeekOpenHour> getWeekHourCache() throws Exception{
		
		// get current time
        java.util.Date date = new java.util.Date();
		
        // get class loader path
        String jsonFilePath = Config.getBaseUrl() + "regularOpenHour.json";
        
        // config the json url from property file
        if (jsonurl == null || jsonurl.equals("")){
        	try {
    			
        		//load a json file path
        		URL jsonURL;
        		jsonURL = new URL(jsonFilePath);
    			jsonurl = jsonURL.toString();
        		System.out.println(jsonurl);
        		
        	} catch (IOException ex) {
        		ex.printStackTrace();
        		log.debug("read property file error : " + ex.toString());
        	}
        }
        
				
		if (((date.getTime() - timestamp) / 1000) > timeSpan || timestamp == 0){
		
			// lapse time is over 15 minutes, reload the data from http server 
			placesHours = new HashMap<String, WeekOpenHour>();
			Gson gson = new Gson();
			String json = getJSONObjectFromUrl(jsonurl);
			JsonObject root = new JsonParser().parse(json).getAsJsonObject();
			for (Entry<String, JsonElement> en : root.entrySet()){
				WeekOpenHour weekhour = gson.fromJson(en.getValue(), WeekOpenHour.class);
				placesHours.put(en.getKey(), weekhour);
			}
			timestamp = date.getTime();
			
			return placesHours;
					
		} else {
			
			// the lapse time is not over 15 minutes, use the cache data to save time for communication
			//System.out.println("lapse = "+ ((date.getTime() - timestamp) / 1000));
        	return placesHours;
		
		}
				
	}
	
	/**
	 * test the method
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		HashMap<String, WeekOpenHour> placesHours = WeekHourAdapter.getWeekHourCache();
		HashMap<String, Place> places = PlaceAdapter.getPlaceCache();
		
		
		for (String placeId : placesHours.keySet()){
			Place p = places.get(placeId);
			System.out.println("    " + p.getId() + "  " + p.getName());
			POI poi = new POI(p, placesHours.get(p.getId() + "").ConvertToWeekBlock());
			System.out.println("    is open = " + poi.isOpen());
		}
		
		/*
		for (String key : placesHours.keySet()){
			System.out.println("    place id = " + key);
			placesHours.get(key).ConvertToWeekBlock();
			for (OpenHourBlock hour : placesHours.get(key).ConvertToWeekBlock().get("Mon")){
				System.out.println("    Monday start time = " + hour.getStartHour() + ":" + hour.getStartMin());
				System.out.println("    Monday end time = " + hour.getEndHour() + ":" + hour.getEndMin());
			}
		}
		*/
		
	}
	
}
