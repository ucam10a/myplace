package edu.uga.eits.portal.portlet.myPlacePortlet.dao;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.uga.eits.portal.portlet.myPlacePortlet.model.Config;

/**
 * consume the xml file and cache the application context for increase performance
 * @author ucam10a
 *
 */
public class ApplicationContextCache {

	/**
	 * declare the server
	 */
	public final static String env = Config.getEnv();  // local, dev, stage, prod
	
	public static String DB_CONFIG = "db-config.xml";
	
	public static String DB_CONFIG_TEST = "db-config-localtest.xml";
	
	/**
	 * check the table exist or not, then return the correspond configure application context
	 * @param TableName the table name
	 * @return the correspond configure application context
	 * @throws Exception 
	 */
	public static ClassPathXmlApplicationContext getApplicationContext(String TableName) {
    
		return new ClassPathXmlApplicationContext(ApplicationContextCache.DB_CONFIG);
	
	}
	
}
