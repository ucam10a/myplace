package edu.uga.eits.portal.portlet.myPlacePortlet.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import edu.uga.eits.portal.portlet.myPlacePortlet.model.MyPlace;

/**
 * @author ucam10a
 * 
 * data acceess object for spring framework and hibernate data acceess object
 * it offers the basic query and update function for Myplace table which is used
 * to store the user favorite place information
 * 
 */
public class HibernateMyPlaceConfigurationDao extends HibernateDaoSupport {

	
	private static Log log = LogFactory.getLog(HibernateMyPlaceConfigurationDao.class);
    
	/**
	 * insert and update my place
	 * @param placeList
	 */
    public void storePlaceConfiguration(List<MyPlace> placeList) {
    	try {

    		for (MyPlace place : placeList){
    			getHibernateTemplate().saveOrUpdate(place);
                getHibernateTemplate().flush();
    		}

        } catch (HibernateException ex) {
            throw convertHibernateAccessException(ex);
        }
    }

    /**
     * delete all my place
     * @param ugaCAN
     */
    public void deleteAllPlaceConfiguration(String ugaCAN) {

    	try {
            
    		List<MyPlace> places = getUserMyPlaceConfiguration(ugaCAN);
            getHibernateTemplate().deleteAll(places);
            getHibernateTemplate().flush();

        } catch (HibernateException ex) {
            throw convertHibernateAccessException(ex);
        }
    }
    
    /**
     * delete my place by its id
     * @param id
     */
    public void deletePlaceConfiguration(String id) {

    	try {
            
    		List<MyPlace> places = getPlaceConfiguration(id);
            getHibernateTemplate().deleteAll(places);
            getHibernateTemplate().flush();

        } catch (HibernateException ex) {
            throw convertHibernateAccessException(ex);
        }
    }
    
    /**
     * get all user's my place by his uga can number
     * @param ugaCAN
     * @return
     */
    @SuppressWarnings("unchecked")
	public List<MyPlace> getUserMyPlaceConfiguration(String ugaCAN) {
        try {
        	
            log.debug("fetching user myPlace for " + ugaCAN);
            return (List<MyPlace>) getHibernateTemplate().find(
                    "from MyPlace where ugaCAN = ? ", ugaCAN);
        
        } catch (HibernateException ex) {
            throw convertHibernateAccessException(ex);
        }
    }

    /**
     * get one my place by its id
     * @param id
     * @return
     */
    @SuppressWarnings("unchecked")
	public List<MyPlace> getPlaceConfiguration(String id) {
        try {
        	
            log.debug("fetching place for " + id);
            return (List<MyPlace>) getHibernateTemplate().find(
                    "from MyPlace where id = ? ", id);
        
        } catch (HibernateException ex) {
            throw convertHibernateAccessException(ex);
        }
    }
    
}
