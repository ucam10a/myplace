package edu.uga.eits.portal.portlet.myPlacePortlet.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.uga.eits.portal.portlet.myPlacePortlet.adapter.PlaceAdapter;
import edu.uga.eits.portal.portlet.myPlacePortlet.adapter.WeekHourAdapter;
import edu.uga.eits.portal.portlet.myPlacePortlet.model.MyPlace;
import edu.uga.eits.portal.portlet.myPlacePortlet.model.POI;
import edu.uga.eits.portal.portlet.myPlacePortlet.model.Place;
import edu.uga.eits.portal.portlet.myPlacePortlet.model.WeekOpenHour;
import edu.uga.eits.portal.portlet.myPlacePortlet.mvc.portlet.MainController;

/**
 * for test dao connection
 * @author ucam10a
 *
 */
public class LocalhostTestDB {
	
	public static void main(String[] args) throws Exception {
    	
		int testTimes = 40;
		long start = System.currentTimeMillis();
		
		for (int i = 0; i < testTimes; i++) {

			ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(ApplicationContextCache.DB_CONFIG_TEST);

			try {

				// get user ugaCAN
				String ugaCAN = "Steven Student";

				HibernateMyPlaceConfigurationDao dao = (HibernateMyPlaceConfigurationDao) ctx
						.getBean("HibernateMyPlaceConfigurationDao");

				HashMap<String, WeekOpenHour> placesHours = WeekHourAdapter
						.getWeekHourCache();
				HashMap<String, Place> places = PlaceAdapter.getPlaceCache();

				// delete all usre customized place id first
				// dao.deleteAllPlaceConfiguration(ugaCAN);

				MyPlace myplace1 = new MyPlace();
				myplace1.setUgaCAN(ugaCAN);
				myplace1.setPlaceId("160");

				MyPlace myplace2 = new MyPlace();
				myplace2.setUgaCAN(ugaCAN);
				myplace2.setPlaceId("195");

				List<MyPlace> list = new ArrayList<MyPlace>();
				list.add(myplace1);
				list.add(myplace2);

				// check first time
				list = MainController.checkFirstTime(list, ugaCAN, places, dao);

				// store my place
				dao.storePlaceConfiguration(list);

				ArrayList<POI> mypois = new ArrayList<POI>();
				for (MyPlace myplace : dao.getUserMyPlaceConfiguration(ugaCAN)) {

					if (!myplace.getPlaceId().equals("-1")) {
						System.out.println("id = " + myplace.getId());
						System.out.println("ugaCAN = " + myplace.getUgaCAN());
						System.out.println("placeId = " + myplace.getPlaceId());

						mypois.add(new POI(places.get(myplace.getPlaceId()),
								placesHours.get(myplace.getPlaceId())
										.ConvertToWeekBlock()));
					}

				}

				for (POI poi : mypois) {

					System.out.println("poi name = " + poi.getPlace().getName());
					System.out.println("    isOpen = " + poi.isOpen());

				}

				// dao.deleteAllPlaceConfiguration(ugaCAN);

				ctx.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
    	
		long end = System.currentTimeMillis();
		System.out.println("lapse = " + (end - start));
		
    	
    }    // main()
	
}
