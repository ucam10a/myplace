package edu.uga.eits.portal.portlet.myPlacePortlet.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Yung Long Li
 * 
 * The attribute of the people
 *
 */
public class Attribute {

	private String attributeName;
	
	private String value;
	
	private List<String> list;

	public Attribute(){
		// empty
	}
	
	public Attribute(String attributeName, String value){
		this.attributeName = attributeName;
		this.value = value;
	}
	
	public Attribute(String attributeName, List<String> list) {
		this.attributeName = attributeName;
		this.list = list;
	}
	
	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(ArrayList<String> list) {
		this.list = list;
	}

}
