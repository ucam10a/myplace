package edu.uga.eits.portal.portlet.myPlacePortlet.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

public class Config {

	private static String baseUrl;
	private static String env;
	
	public static String getBaseUrl(){
		if (baseUrl == null || baseUrl.equals("")){
			//get the property value and print it out
	        baseUrl = getPorperty("jsonURLPath");
	    	return baseUrl; 
	    }else{
			return baseUrl;
		}
	}
	
	public static String getEnv() {
		if (env == null || env.equals("")){
			//get the property value and print it out
			env = getPorperty("env");
	    	return env; 
	    }else{
			return env;
		}
	}
	
	private static String getPorperty(String key){
		
		String value = "";
		
		Properties prop = new Properties();
		URL url = Config.class.getClassLoader().getResource("");
        String propFilePath = url + "configuration.properties";
		
		try {
			
			File propFile = new File(new URL(propFilePath).toURI());
	           
			//load a properties file
    		prop.load(new FileInputStream(propFile));
 
            //get the property value and print it out
    		value = prop.getProperty(key);
    		
            if (value != null) {
            	return value; 
            }else{
                return "";
            }
            
    	} catch (IOException ex) {
    		ex.printStackTrace();
        } catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
}
