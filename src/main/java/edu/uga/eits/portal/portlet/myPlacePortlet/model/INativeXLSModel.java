package edu.uga.eits.portal.portlet.myPlacePortlet.model;

import java.util.ArrayList;

import jxl.Workbook;

public interface INativeXLSModel {

	public void SetObject(ArrayList<String> contents, ArrayList<String> attributeNames) throws Exception;
	
	public ArrayList<String> getAttribute(Workbook book, int sheetNum, int rowNum);
	
	public void constructObject(Workbook book, int sheetNum, int AttributeRowNum, int rowNum) throws Exception; 
		
}
