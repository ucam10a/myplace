package edu.uga.eits.portal.portlet.myPlacePortlet.model;

/**
 * 
 * @author ucam10a
 *
 * the data orm for storing favorite place in database
 */
public class MyPlace {
	
	private long id;
	
	/**
	 * user unique id
	 */
	private String ugaCAN;
	
	/**
	 * place unique id
	 */
	private String placeId;

	public MyPlace(){
		
	}
	
	public MyPlace(String placeId, String ugaCAN){
		this.placeId = placeId;
		this.ugaCAN = ugaCAN;
	}
	
	public String getUgaCAN() {
		return ugaCAN;
	}

	public void setUgaCAN(String ugaCAN) {
		this.ugaCAN = ugaCAN;
	}

	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	
}
