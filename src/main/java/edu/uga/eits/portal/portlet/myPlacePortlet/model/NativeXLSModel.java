package edu.uga.eits.portal.portlet.myPlacePortlet.model;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

/**
 * This native model is used to convert from xsl to Java Object easily 
 * @author ucam10a
 *
 */
public abstract class NativeXLSModel implements INativeXLSModel {
	
	private ArrayList<String> attributes = new ArrayList<String>();
	
	/**
	 * match the field and setup the object automatically
	 * @param contents the String array list from list of data(attributes and contents) 
	 * @param attributeNames the String array list from list of data() 
	 * @throws Exception
	 */
	public void SetObject(ArrayList<String> contents, ArrayList<String> attributeNames) throws Exception {
		
		ArrayList<String> temp = new ArrayList<String>();
        for (String attr : attributeNames) temp.add(attr);
        
		// match the field and method
		Method[] methods = getClass().getDeclaredMethods();
		ArrayList<Method> methodList = new ArrayList<Method>();
		for (String str : attributeNames){
			for (Method method : methods) {
				if (method.getName().equalsIgnoreCase("set" + str)){
					Type[] paramTypes = method.getGenericParameterTypes();
					if (paramTypes.length == 1 && paramTypes[0].toString().equals("class java.lang.String")){
						methodList.add(method);
						temp.remove(str);
						break;
					} else {
						
					}
				}
			}
		}
		
		// check the error
		if (methodList.size() > attributeNames.size()){
			
			System.out.println("methodList size = " + methodList.size());
			System.out.println("attributeNames size = " + attributeNames.size());
			
			throw new Exception("Missing parameter in xls attributes !");
		}else if (methodList.size() < attributeNames.size()){
			
			System.out.println("methodList size = " + methodList.size());
			System.out.println("attributeNames size = " + attributeNames.size());
			
			String missing = "";
			for (String attr : temp) missing += " " + attr;
			throw new Exception("xls has more parameters <" + missing + "> than Class Object Fields!");
		}
		
		if (methodList.size() != attributeNames.size()){
			
			System.out.println("methodList size = " + methodList.size());
			System.out.println("attributeNames size = " + attributeNames.size());
			
			String attrs = "";
			for (String attr : temp) attrs = attrs + "<" +  attr + ">";
			throw new Exception("Not implement on fields:" + attrs + " for setting String input parameter!");
		}
		
		int i = 0;
		for (String str : contents) {
			if (i >= methodList.size()) break;
			methodList.get(i).invoke(this, str);
			i++;
		}
		
	}
	
	/**
	 * get ArrayList String of attributes  
	 * @param book the workbook object of excel
	 * @param sheetNum the number of worksheet of excel
	 * @param rowNum the row number of attribute
	 * @return the list of attribute
	 */
	public ArrayList<String> getAttribute(Workbook book, int sheetNum, int rowNum) {
		
		return getAttribute(book, sheetNum, rowNum, 0);
		
	}
	
	/**
	 * get ArrayList String of attributes  
	 * @param book the workbook object of excel
	 * @param sheetNum the number of worksheet of excel
	 * @param rowNum the row number of attribute
	 * @param startColNum the start column number
	 * @return the list of attribute
	 */
	public ArrayList<String> getAttribute(Workbook book, int sheetNum, int rowNum, int startColNum) {
		
		ArrayList<String> attributes = new ArrayList<String>();
		
		Sheet sheet = book.getSheet(sheetNum);
		// get the attribute name list
		int colIdx = 0;
		for (Cell c : sheet.getRow(rowNum)){
			if (colIdx >= startColNum && !c.getContents().trim().equals(""))attributes.add(c.getContents().trim());
			colIdx++;
		}
		
		return attributes;
	}
	
	/**
	 * set every value in the object
	 * @param book the workbook object of excel
	 * @param sheetNum the worksheet number of excel
	 * @param AttributeRowNum the row number of attribute in excel sheet 
	 * @param rowNum the row number which contains the record content
	 * @throws Exception
	 */
	public void constructObject(Workbook book, int sheetNum, int AttributeRowNum, int startRowNum) throws Exception {
		
		constructObject(book, sheetNum, AttributeRowNum, startRowNum, 0);
		
	}
	
	/**
	* set every value in the object
	 * @param book the workbook object of excel
	 * @param sheetNum the worksheet number of excel
	 * @param AttributeRowNum the row number of attribute in excel sheet 
	 * @param startRowNum the row number which contains the record content
	 * @param startColNum the column start number for the content, for example skip the first column as the key
	 * @throws Exception
	 */
	public void constructObject(Workbook book, int sheetNum, int AttributeRowNum, int startRowNum, int startColNum) throws Exception {
		
		Sheet sheet = book.getSheet(sheetNum);
		if (attributes.size() == 0){
			attributes = getAttribute(book, sheetNum, AttributeRowNum, startColNum);
		}
		
		int colIdx = 0;
		ArrayList<String> contents = new ArrayList<String>();
		for (Cell c : sheet.getRow(startRowNum)){
			if (colIdx >= startColNum) contents.add(c.getContents().trim());
			colIdx++;
		}
		
		SetObject(contents, attributes);
		
	}
	
	
}
