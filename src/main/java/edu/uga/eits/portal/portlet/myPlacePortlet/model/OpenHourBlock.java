package edu.uga.eits.portal.portlet.myPlacePortlet.model;

/**
 * define the open hour and minute for time block
 * @author ucam10a
 *
 */
public class OpenHourBlock {

	private int startHour = 25;
	
	private int endHour = -1;
	
	private int startMin = 61;
	
	private int endMin = -1;
	
	private String continueBlock = "";

	public OpenHourBlock(int startHour, int startMin, int endHour, int endMin) throws Exception {
		
		this.setStartHour(startHour);
		this.setEndHour(endHour);
		this.setStartMin(startMin);
		this.setEndMin(endMin);

	}

	public int getStartHour() {
		return startHour;
	}

	public void setStartHour(int startHour) throws Exception {
		if (startHour < 0 || startHour > 23) throw new Exception("wrong hour time format");
		this.startHour = startHour;
	}

	public int getEndHour() {
		return endHour;
	}

	public void setEndHour(int endHour) throws Exception {
		if (endHour < 0 || endHour > 23) throw new Exception("wrong hour time format");
		this.endHour = endHour;
	}

	public int getStartMin() {
		return startMin;
	}

	public void setStartMin(int startMin) throws Exception {
		if (startMin < 0 || startMin > 59) throw new Exception("wrong minute time format");
		this.startMin = startMin;
	}

	public int getEndMin() {
		return endMin;
	}

	public void setEndMin(int endMin) throws Exception {
		if (endMin < 0 || endMin > 59) throw new Exception("wrong minute time format");
		this.endMin = endMin;
	}

	
	public String getContinueBlock() {
		return continueBlock;
	}

	public void setContinueBlock(String continueBlock) {
		this.continueBlock = continueBlock;
	}
	
}
