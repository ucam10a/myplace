package edu.uga.eits.portal.portlet.myPlacePortlet.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import edu.uga.eits.portal.portlet.myPlacePortlet.adapter.PlaceAdapter;
import edu.uga.eits.portal.portlet.myPlacePortlet.adapter.WeekHourAdapter;

/**
 * 
 * @author ucam10a
 *
 * the basic object for store all information about one place.
 * The information include place information and open hour information 
 */
public class POI {

	/**
	 * The place 
	 */
	private Place place;
	
	/**
	 * open hour
	 */
	private HashMap<String, ArrayList<OpenHourBlock>> openHourSchedule;
	
	/**
	 * current is open
	 */
	private boolean open = false;
	
	/**
	 * selected by user
	 */
	private boolean select = false;

	/** The attributes of poi */
	private ArrayList<Attribute> attributes = new ArrayList<Attribute>();
	
	public POI(){
		// empty constructor
	}
	
	public POI (Place place, HashMap<String, ArrayList<OpenHourBlock>> openHourSchedule) {
		
		this.place = place;
		
		// delete the building number 
		if (place.getName().contains("#")) place.setName(place.getName().split("#")[0]);
		
		this.setOpenHourSchedule(openHourSchedule);
		
		// get the current time
		Calendar rightNow = Calendar.getInstance();
		
		this.setOpen(checkOpen(rightNow, openHourSchedule));

		// set attribute for display
		attributes.add(new Attribute("Today's Hours", getSchedule(rightNow, openHourSchedule)));
		attributes.add(new Attribute("call", ""));
		attributes.add(new Attribute("email", ""));
		attributes.add(new Attribute("address", place.getAddress() + ", " + place.getCity() + " " + place.getState()));
		
	}
	
	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public boolean checkOpen(Calendar rightNow, HashMap<String, ArrayList<OpenHourBlock>> openHourSchedule) {
		
		String DayOfWeek = getTodayDayOfWeek(rightNow);
		int hour = rightNow.get(Calendar.HOUR_OF_DAY);
		int min = rightNow.get(Calendar.MINUTE);
		
		ArrayList<OpenHourBlock> daySchedule = openHourSchedule.get(DayOfWeek);
		for (OpenHourBlock block : daySchedule){
			if (hour > block.getStartHour() && hour < block.getEndHour()) return true;
			if (hour == block.getStartHour()){ 
				if (min >= block.getStartMin()) return true;
			}
			if (hour == block.getEndHour()){
				if (min <= block.getEndMin()) return true;
			}
		}
		
		return false;
	
	}
	
	public static String getSchedule(Calendar rightNow, HashMap<String, ArrayList<OpenHourBlock>> openHourSchedule){
		
		String result = getDayOpenTime(rightNow, openHourSchedule);
		
		if (result.contains("11:59PM")) {
			if (result.contains("00:00AM-11:59PM")){
				result = "00:00AM - 11:59PM (All Day Open)";
			}else{
				String ContinuousBlock = getContinuousBlock(rightNow, openHourSchedule);
				if (ContinuousBlock.equals("")){
					//result = result.replaceAll("23:59", "24:00");
				}else{
					if (ContinuousBlock.equals("11:59PM")){
						result = result.replace("11:59PM", "continue to next day");
					}else{
						result = result.replace("11:59PM", ContinuousBlock + "(same day)");
					}
				}
			}
			
		}
		return result;		
	}
	
	public static String getDayOpenTime(Calendar rightNow, HashMap<String, ArrayList<OpenHourBlock>> openHourSchedule){
		
		String result = "";
		String DayOfWeek = getTodayDayOfWeek(rightNow);
		ArrayList<OpenHourBlock> daySchedule = openHourSchedule.get(DayOfWeek);
		for (OpenHourBlock block : daySchedule){
			result = result + changeTimeFormat(block.getStartHour(), block.getStartMin());
			result = result + "-";
			result = result + changeTimeFormat(block.getEndHour(), block.getEndMin());
			result = result + "  ";
		}
		
		if (result.equals("")) result = "close";
		return result;
		
	}
	
	private static String changeTimeFormat(int hour, int min){
		
		String result = "";
		boolean isAM = true;
		
		if (hour == 0) {
			result = result + "00";
		}else{
			if (hour >= 12){
				isAM = false;
			}
			if(hour > 12){
				result = result + (hour - 12);
			}else{
				result = result + hour;
			}
		}
		result = result + ":";
		if (min == 0){
			result = result + "00";
		}else{
			result = result + min;
		}
		if (isAM){
			result = result + "AM";
		}else{
			result = result + "PM";
		}
		
		
		return result;
	}

	public static String getTodayDayOfWeek(Calendar rightNow) {
		
		return getDayOfWeek(rightNow.get(Calendar.DAY_OF_WEEK));
		
	}
	
	private static String getTomorrowDayOfWeek(Calendar rightNow) {
		
		int tomorrow = rightNow.get(Calendar.DAY_OF_WEEK) + 1;
		if (tomorrow == 8) tomorrow = 1;
		return getDayOfWeek(tomorrow);
		
	}
	
	public static String getDayOfWeek(int day) {
		
		String DayOfWeek = null;
		
		switch (day) {
		case Calendar.SUNDAY:
			DayOfWeek = "Sun";
			break;
		case Calendar.MONDAY:
			DayOfWeek = "Mon";
			break;
		case Calendar.TUESDAY:
			DayOfWeek = "Tue";
			break;
		case Calendar.WEDNESDAY:
			DayOfWeek = "Wed";
			break;
		case Calendar.THURSDAY:
			DayOfWeek = "Thr";
			break;
		case Calendar.FRIDAY:
			DayOfWeek = "Fri";
			break;
		case Calendar.SATURDAY:
			DayOfWeek = "Sat";
			break;
		}
		
		return DayOfWeek;

		
	}


	private static String getContinuousBlock(Calendar rightNow, HashMap<String, ArrayList<OpenHourBlock>> openHourSchedule) {
		
		String result = "";
		String tomorrowDayofWeek = getTomorrowDayOfWeek(rightNow);
		ArrayList<OpenHourBlock> daySchedule = openHourSchedule.get(tomorrowDayofWeek);
		for (OpenHourBlock block : daySchedule){
			if (block.getStartHour() == 0 && block.getStartMin() == 0) {
				result = result + changeTimeFormat(block.getEndHour(), block.getEndMin());
				break;
			}
		}
		
		return result;
	}
	
	public HashMap<String, ArrayList<OpenHourBlock>> getOpenHourSchedule() {
		return openHourSchedule;
	}


	public void setOpenHourSchedule(HashMap<String, ArrayList<OpenHourBlock>> openHourSchedule) {
		this.openHourSchedule = openHourSchedule;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public ArrayList<Attribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(ArrayList<Attribute> attributes) {
		this.attributes = attributes;
	}

	public boolean isSelect() {
		return select;
	}

	public void setSelect(boolean select) {
		this.select = select;
	}
	
	/**
	 * test the method
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		HashMap<String, WeekOpenHour> placesHours = WeekHourAdapter.getWeekHourCache();
		HashMap<String, Place> places = PlaceAdapter.getPlaceCache();
		
		String placeId = "165";
		POI poi = new POI(places.get(placeId), placesHours.get(placeId).ConvertToWeekBlock());
		
		System.out.println("name = " + poi.getPlace().getName());
		System.out.println("lat = " + poi.getPlace().getLatitude() + ":lon = " + poi.getPlace().getLongitude());
		for (Attribute attr : poi.getAttributes()){
			System.out.println("    "  + attr.getAttributeName() + "  " + attr.getValue());
		}
		
	}
	
}
