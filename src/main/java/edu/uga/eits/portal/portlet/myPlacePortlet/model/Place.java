package edu.uga.eits.portal.portlet.myPlacePortlet.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Long
 * 
 * describe the location on campus
 */
public class Place extends NativeXLSModel {

	/**
	 * unique id
	 */
	private int id = 0; 
	
	/**
	 * location name
	 */
	private String name = "";
	
	/**
	 * abbreviation of name, usually upper case
	 */
	private String abbreviation = "";
	
	/**
	 * the address of location, ex. "North Frontage Road New Haven, CT 06511 New Haven, CT 06511"
	 */
	private String address = "";
	
	/**
	 * located city
	 */
	private String city = "";
	
	/**
	 * located state
	 */
	private String state = "";
	
	/**
	 * located zip code
	 */
	private String zip = "";
	
	/**
	 * located country
	 */
	private String country = "";
	
	/**
	 * latitude
	 */
	private double latitude = 0.0D;
	
	/**
	 * longitude
	 */
	private double longitude = 0.0D;
	
	/**
	 * the URL of the image icon for this location, ex. "http://business.yale.edu/map/thumbnail/ARG.jpg"
	 */
	private String img = "";
	
	/**
	 * the search text that used for searching
	 */
	private String searchText = "";
	
	/**
	 * search keys, it is an array, now it is not used
	 */
	private List<String> searchKeys; 
	
	/**
	 * categories, it is an array of String. ex. "food;library"
	 */
	private List<String> categories;
	
	/**
	 * campuses, it is an array of String. ex. "main"
	 */
	private List<String> campuses;
	
	/**
	 * display in myPlace or not 
	 */
	private boolean displaymyPlace = false;
	
	/**
	 * UGA building code
	 */
	private String code = "";
	
	/**
	 * 
	 */
	private int in_tour = 0;
	
	/**
	 * building information
	 */
	private String info = "";
	
	/**
	 * 
	 */
	private int tour_position = -1;
	
	/**
	 * place icon id
	 */
	private int icon_id = -1;
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public void setLatitude(String latitude) {
		double lat = Double.parseDouble(latitude);
		this.latitude = lat;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public void setLongitude(String longitude) {
		double lon = Double.parseDouble(longitude);
		this.longitude = lon;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress() {
		return address;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getImg() {
		return img;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getSearchText() {
		return searchText;
	}

	/**
	 * set up location searchkeys 
	 * @param c excel cell
	 */
	public void setSearchKeys(String content) {
		List<String> searchKeys = new ArrayList<String>();
		String [] strKeys = content.split(";");
		for (String key : strKeys){
			searchKeys.add(key.trim());
		}
		this.searchKeys = searchKeys;
	}
	
	public List<String> getSearchKeys() {
		return searchKeys;
	}
	
	public List<String> getCategories() {
		return categories;
	}	

	public void setCategories(String content) {
		List<String> categories = new ArrayList<String>();
		String [] strCates = content.split(";");
		for (String cate : strCates){
			categories.add(cate.trim());
		}
		this.categories = categories;
	}
	
	public List<String> getCampuses() {
		return campuses;
	}
	
	public void setCampuses(String content) {
		List<String> campuses = new ArrayList<String>();
		String [] strCampuses = content.split(";");
		for (String camp : strCampuses){
			campuses.add(camp.trim());
		}
		this.campuses = campuses;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setId(String idStr) {
		this.id = Integer.parseInt(idStr);
	}
	
	public int getId() {
		return id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getIn_tour() {
		return in_tour;
	}

	public void setIn_tour(int in_tour) {
		this.in_tour = in_tour;
	}
	
	public void setIn_tour(String in_tourStr) {
		try {
			this.in_tour = Integer.parseInt(in_tourStr);
		}catch (Exception e){
			// do nothing
		}
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getTour_position() {
		return tour_position;
	}

	public void setTour_position(int tour_position) {
		this.tour_position = tour_position;
	}

	public void setTour_position(String tour_positionStr) {
		try {
			this.tour_position = Integer.parseInt(tour_positionStr);
		}catch (Exception e){
			// do nothing
		}
	}
	
	public int getIcon_id() {
		return icon_id;
	}

	public void setIcon_id(int icon_id) {
		this.icon_id = icon_id;
	}
	
	public void setIcon_id(String icon_idStr) {
		try {
			this.icon_id = Integer.parseInt(icon_idStr);
		}catch (Exception e){
			// do nothing
		}
	}

	public boolean isDisplaymyPlace() {
		return displaymyPlace;
	}

	public void setDisplaymyPlace(boolean displaymyPlace) {
		this.displaymyPlace = displaymyPlace;
	}
	
	public void setDisplaymyPlace(String isDisplaymyPlaceStr) {
		if (isDisplaymyPlaceStr.equalsIgnoreCase("true")){
			this.displaymyPlace = true;
		} else {
			this.displaymyPlace = false;
		}
	}
}
