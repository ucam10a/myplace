package edu.uga.eits.portal.portlet.myPlacePortlet.model;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * 
 * @author ucam10a
 * The basic model to describe the open hour for a week. It is also used to store or convert to json file.
 * it describes the open hour in every day, the format is like, ex. "0800-1200;1700:2000"
 * It means that there are two blocks, one is from 8AM to 12AM, the other is 5PM to 8PM.
 */
public class WeekOpenHour extends NativeXLSModel {

	/**
	 * Monday open hour
	 */
	private String Mon = "";
	
	/**
	 * Tuesday open hour
	 */
	private String Tue = "";
	
	/**
	 * Wednesday
	 */
	private String Wed = "";
	
	/**
	 * Thursday open hour
	 */
	private String Thr = "";
	
	/**
	 * Friday open hour
	 */
	private String Fri = "";
	
	/**
	 * Saturday open hour
	 */
	private String Sat = "";
	
	/**
	 * Sunday open hour
	 */
	private String Sun = "";
	
	/**
	 * convert the all day hour to a hash map object 
	 * @return hash map
	 * @throws Exception
	 */
	public HashMap<String, ArrayList<OpenHourBlock>> ConvertToWeekBlock() throws Exception {
		
		HashMap<String, ArrayList<OpenHourBlock>> weekBlock = new HashMap<String, ArrayList<OpenHourBlock>>();
		Field[] fields = getClass().getDeclaredFields();
		Method[] methods = getClass().getMethods();
		
		// match method 
		HashMap<Field, Method> methodMap = new HashMap<Field, Method>();
		for (Field field : fields) {
			for (Method method : methods){
				if (method.getName().equalsIgnoreCase("get" + field.getName())){
					methodMap.put(field, method);
					break;
				}
			}
		}
		
		for (Field field : fields) {
			String text = methodMap.get(field).invoke(this).toString();
			ArrayList<OpenHourBlock> blocks = new ArrayList<OpenHourBlock>();
			String[] blocksStr = text.split(";");
			if (blocksStr != null && blocksStr.length > 0){
				for (String block : blocksStr){
					String[] times = block.trim().split("-");
					if (times != null && times.length > 1){
						String startTime = times[0].trim();
						String endTime = times[1].trim();
						int startHour = Integer.parseInt(startTime.substring(0, 2));
						int startMin = Integer.parseInt(startTime.substring(2, 4));
						int endHour = Integer.parseInt(endTime.substring(0, 2));
						int endMin = Integer.parseInt(endTime.substring(2, 4));
						OpenHourBlock timeBlock = new OpenHourBlock(startHour, startMin, endHour, endMin);
						blocks.add(timeBlock);
					}
				}
			}
			weekBlock.put(field.getName(), blocks);
		}
		
		return weekBlock;
	}

	public String getMon() {
		return Mon;
	}

	public void setMon(String mon) {
		Mon = mon;
	}

	public String getTue() {
		return Tue;
	}

	public void setTue(String tue) {
		Tue = tue;
	}

	public String getWed() {
		return Wed;
	}

	public void setWed(String wed) {
		Wed = wed;
	}

	public String getThr() {
		return Thr;
	}

	public void setThr(String thr) {
		Thr = thr;
	}

	public String getFri() {
		return Fri;
	}

	public void setFri(String fri) {
		Fri = fri;
	}

	public String getSat() {
		return Sat;
	}

	public void setSat(String sat) {
		Sat = sat;
	}

	public String getSun() {
		return Sun;
	}

	public void setSun(String sun) {
		Sun = sun;
	}
	
}
