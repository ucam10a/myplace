/**
 * Licensed to Jasig under one or more contributor license
 * agreements. See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Jasig licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a
 * copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package edu.uga.eits.portal.portlet.myPlacePortlet.mvc.portlet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import edu.uga.eits.portal.portlet.myPlacePortlet.adapter.PlaceAdapter;
import edu.uga.eits.portal.portlet.myPlacePortlet.adapter.WeekHourAdapter;
import edu.uga.eits.portal.portlet.myPlacePortlet.dao.ApplicationContextCache;
import edu.uga.eits.portal.portlet.myPlacePortlet.dao.HibernateMyPlaceConfigurationDao;
import edu.uga.eits.portal.portlet.myPlacePortlet.model.MyPlace;
import edu.uga.eits.portal.portlet.myPlacePortlet.model.POI;
import edu.uga.eits.portal.portlet.myPlacePortlet.model.Place;
import edu.uga.eits.portal.portlet.myPlacePortlet.model.WeekOpenHour;
import edu.uga.eits.portal.portlet.myPlacePortlet.mvc.IViewSelector;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.ModelAndView;

/**
 * Main portlet view.
 */
@Controller
@RequestMapping("VIEW")
public class MainController {

    protected final Log logger = LogFactory.getLog(getClass());
    
    private IViewSelector viewSelector;
    
    @Autowired(required = true)
    public void setViewSelector(IViewSelector viewSelector) {
        this.viewSelector = viewSelector;
    }
    
    //@Autowired
    //private HibernateMyPlaceConfigurationDao configDao;
    
    @RenderMapping
    /**
     * get the user selected favorite places for jsp to show
     * @param request render request
     * @param response render response
     * @return view
     * @throws Exception
     */
    public ModelAndView showMainView(final RenderRequest request, final RenderResponse response) throws Exception {
    	
    	if (ApplicationContextCache.env.equals("local")) System.out.println("action = null");
    	
        // determine if the request represents a mobile browser and set the
        // view name accordingly
        final boolean isMobile = viewSelector.isMobile(request);
        final String viewName = isMobile ? "main-jQM" : "main";        
        
        final Map<String,Object> model = new HashMap<String, Object>();
        
        //Get the USER_INFO from portlet.xml,
        //which gets it from personDirectoryContext.xml
        @SuppressWarnings("unchecked")
        final Map<String, String> userInfo = (Map<String, String>) request.
                getAttribute(PortletRequest.USER_INFO);
        
        try {
        	
        	// get user ugaCAN
        	String ugaCAN = userInfo.get("employeeNumber");
        	if (ApplicationContextCache.env.equals("local")) System.out.println("ugaCAN = " + ugaCAN);
			model.put("ugaCAN", ugaCAN);
        	model.put("displayName", ugaCAN);
        	
        	// get place and hour schedule cache
			HashMap<String, WeekOpenHour> placesHours = WeekHourAdapter.getWeekHourCache();
			HashMap<String, Place> places = PlaceAdapter.getPlaceCache();
        	
			// create my pois
			ArrayList<POI> mypois = new ArrayList<POI>();
			
			if (ugaCAN != null && !ugaCAN.equals("")) {
        		
        		// spring frame work and hiberate setting
        		ClassPathXmlApplicationContext ctx = ApplicationContextCache.getApplicationContext("MYPLACE");
            	HibernateMyPlaceConfigurationDao dao = (HibernateMyPlaceConfigurationDao)ctx.getBean("HibernateMyPlaceConfigurationDao");
            	if(logger.isDebugEnabled()) {
    	            logger.debug("main: connect to database successfully!");
    	        }
            	
    			if(logger.isDebugEnabled()) {
    	            logger.debug("main: get all place cache size = " + places.size());
    	            logger.debug("main: get all open hours cache size = " + placesHours.size());
    	        }
    			
    			// get user customized place id from database
    			List<MyPlace> myplaces = dao.getUserMyPlaceConfiguration(ugaCAN);
    			
    			// check first time to use and remove the record (-1, ugaCAN)
    			myplaces = checkFirstTime(myplaces, ugaCAN, places, dao);
    			
    			if(logger.isDebugEnabled()) {
    	            logger.debug("main: get data from database");
    	        }
    			
    			// create the user pois and check the open hour while create the POI
    			for (MyPlace myplace : myplaces){
    				if (!myplace.getPlaceId().equals("-1")){
    					mypois.add(new POI(places.get(myplace.getPlaceId()), placesHours.get(myplace.getPlaceId()).ConvertToWeekBlock()));
    	    		}
    			}
    			model.put("mypois", mypois);
            	
    			if(logger.isDebugEnabled()) {
                	logger.debug("main: Rendering main view");
            	}
            	
    			ctx.close();
    			
    			if (ApplicationContextCache.env.equals("local")) {
    				System.out.println("ugaCAN = " + ugaCAN);
    				System.out.println("get all cache");
    				System.out.println("mypois size = " + mypois.size());
    			}
    			
        	}else {
        		
        		// get all myplaces
    			for (String placeId : placesHours.keySet()){
    				mypois.add(new POI(places.get(placeId), placesHours.get(placeId).ConvertToWeekBlock()));
    	    	}
        		model.put("mypois", mypois);
        		
        	}
        	
        } catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR: " + e.toString());
		}
        
        if(logger.isDebugEnabled()) {
            logger.debug("main: Using view name " + viewName + " for main view");
        }
        
        return new ModelAndView(viewName, model);

    }
    
    @RenderMapping(params="action=editForm")
    /**
     * get the place for jsp to show the select form
     * @param request render request
     * @param response render response
     * @return view
     */
    public ModelAndView showFormView(
            final RenderRequest request, final RenderResponse response) {
    	
    	if (ApplicationContextCache.env.equals("local")) System.out.println("action = editForm");
    	
        // determine if the request represents a mobile browser and set the
        // view name accordingly
        final boolean isMobile = viewSelector.isMobile(request);
        final String viewName = isMobile ? "edit-jQM" : "edit";        
        
        final Map<String,Object> model = new HashMap<String, Object>();
        
        //Get the USER_INFO from portlet.xml,
        //which gets it from personDirectoryContext.xml
        @SuppressWarnings("unchecked")
        final Map<String, String> userInfo = (Map<String, String>) request.
                getAttribute(PortletRequest.USER_INFO);
        
        try {
            
        	// get user ugaCAN
        	String ugaCAN = userInfo.get("employeeNumber");
        	model.put("ugaCAN", ugaCAN);
        	
        	// for test only
        	if (ApplicationContextCache.env.equals("local")) ugaCAN = "810111111";
        	
        	if (ugaCAN != null && !ugaCAN.equals("")) {
        		
        		// spring frame work and hiberate setting
        		ClassPathXmlApplicationContext ctx = ApplicationContextCache.getApplicationContext("MYPLACE");
            	HibernateMyPlaceConfigurationDao dao = (HibernateMyPlaceConfigurationDao)ctx.getBean("HibernateMyPlaceConfigurationDao");
            	if(logger.isDebugEnabled()) {
    	            logger.debug("editform: connect to database successfully!");
    	        }
            	
            	// get place cache
    			HashMap<String, Place> places = PlaceAdapter.getPlaceCache();
    			
    			// delete the building number 
    			for (Place p : places.values()){
    				if (p.getName().contains("#")) p.setName(p.getName().split("#")[0]);
    			}
    			
    			if(logger.isDebugEnabled()) {
    	            logger.debug("editform: get all place cache size = " + places.size());
    	        }
    			
    			// get user customized place
    			List<MyPlace> myplaces = dao.getUserMyPlaceConfiguration(ugaCAN);
    			
    			// check first time to use and remove the record (-1, ugaCAN)
    			myplaces = checkFirstTime(myplaces, ugaCAN, places, dao);
    			
    			if(logger.isDebugEnabled()) {
    	            logger.debug("editform: get data from database");
    	        }
    			
    			// selectedPoiId is a value which concatenate the user customized place id, ex. "190, 165, 120"
    			String selectedPoiId = "";
    			ArrayList<POI> mypois = new ArrayList<POI>();
    			for (String placeId : places.keySet()){
    				if (places.get(placeId).isDisplaymyPlace()) {
    					POI poi = new POI();
    					poi.setPlace(places.get(placeId));
    					if (checkSelected(placeId, myplaces)){
    						poi.setSelect(true);
    						selectedPoiId = selectedPoiId + ", " + placeId;
    					}else{
    						poi.setSelect(false);
    					}
    					mypois.add(poi);
    				}
    			}
    			
    			model.put("selectedPoiId", selectedPoiId);
    			model.put("mypois", mypois);
            	
    			if(logger.isDebugEnabled()) {
                	logger.debug("editform: Rendering main view");
            	}
    			
    			// clear action
                model.put("action", "");
    			
    			// close database connection
    			ctx.close();
    			
        	}else {
        		
        		model.put("selectedPoiId", "");
        		model.put("mypois", new ArrayList<POI>());
        		model.put("error", "Please login and try again. If problem remains, you may need a vaild UGA ID.");
        		
        	}
        	
        
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR editform: " + e.toString());
		}	
        	
        if(logger.isDebugEnabled()) {
            logger.debug("editform: Using view name " + viewName + " for edit view");
        }

        return new ModelAndView(viewName, model);

    }

    /**
     * check the place which user has added to his favorite
     * @param placeId the input place id
     * @param myplaces the list of user favorite place id
     * @return true if it is included in user favorite list
     */
    private static boolean checkSelected(String placeId, List<MyPlace> myplaces){
    	
    	for (MyPlace myplace : myplaces){
    		if (placeId.equals(myplace.getPlaceId())) return true;
    	}
    	return false;
    	
    }
    
    
    @ActionMapping
    /**
     * Get the place ids which are selected by user, then store in database
     * @param response response to new render
     * @param selectedPoi String for selected place id ex: 160, 195, 181
     * @param ugaCAN UGA ID
     */
    public void doAction(ActionResponse response, @RequestParam(value = "selectedPoi", required = true) String selectedPoi, @RequestParam(value = "ugaCAN", required = true) String ugaCAN) {
        
    	if (ApplicationContextCache.env.equals("local")) System.out.println("action = editPOI");
    	
        try {
            
        	if (ApplicationContextCache.env.equals("local")) ugaCAN = "810111111";
        	if (ApplicationContextCache.env.equals("local")) System.out.println("ugaCAN = " + ugaCAN);
        	if (ApplicationContextCache.env.equals("local")) System.out.println("selectedPoi = " + selectedPoi);
        	
        	if (ugaCAN != null && !ugaCAN.equals("")) {
        		
        		// spring frame work and hibernate setting
        		ClassPathXmlApplicationContext ctx = ApplicationContextCache.getApplicationContext("MYPLACE");
            	HibernateMyPlaceConfigurationDao dao = (HibernateMyPlaceConfigurationDao)ctx.getBean("HibernateMyPlaceConfigurationDao");
            	if(logger.isDebugEnabled()) {
    	            logger.debug("action: connect to database successfully!");
    	        }
            	
    			// delete all usre customized place id first
    			dao.deleteAllPlaceConfiguration(ugaCAN);
    			
    			// selectedPoiId is a value which concatenate the user customized place id, ex. "190, 165, 120"
    			// split it to an array
    			String[] newMyPoiId = selectedPoi.split(",");
    			
    			// create the new empty ArrayList for insert data to database
    			ArrayList<MyPlace> newMyPlaces = new ArrayList<MyPlace>();
    			for (String newId : newMyPoiId){
    				if (newId != null && !newId.trim().equals("")){
    					if (ApplicationContextCache.env.equals("local")) System.out.println("newId = " + newId);
    					newMyPlaces.add(new MyPlace(newId, ugaCAN));
    				}
    			}
    			
    			// insert the first time indicator
    			newMyPlaces.add(new MyPlace("-1", ugaCAN));
    			
    			// insert to database
    			dao.storePlaceConfiguration(newMyPlaces);
    			
    			if(logger.isDebugEnabled()) {
    	            logger.debug("action: store to database successfully!");
    	        }
    			
    			if(logger.isDebugEnabled()) {
                	logger.debug("Rendering main view");
            	}
    			
    			// close connection
            	ctx.close();
        	}

		} catch (Exception e) {
			if (ApplicationContextCache.env.equals("local")) e.printStackTrace();
			logger.error("ERROR action: " + e.toString());
		}
        
        // return null action value so that browser get default render page(main page)
        response.setRenderParameter("action", "");
        if(logger.isDebugEnabled()) {
            logger.debug("action: send to response");
        }
        
    }
    
    /**
     * check user is first time to use this function, if there exist a record ("-1"m ugaCAN), then not.
     * @param myplaces the list of myplace record
     * @param ugaCAN uga ID
     * @param places place object
     * @param dao data access object for hibernate
     * @return the new list, if user is first time to use it then add all place to this list and store in database. If not, remove the ("-1", ugaCAN) in the list and return
     */
    public static List<MyPlace> checkFirstTime(List<MyPlace> myplaces, String ugaCAN, HashMap<String, Place> places, HibernateMyPlaceConfigurationDao dao){
    	
    	// check if user is first time to use
		if (myplaces.size() == 0){
			
			// store the record (ugaCAN, -1) to indicate the user is not first time
			// and store all place
			List<MyPlace> placeList = new ArrayList<MyPlace>();
			placeList.add(new MyPlace("-1", ugaCAN));
			for (Place p : places.values()){
				myplaces.add(new MyPlace(p.getId() + "", ugaCAN));
				placeList.add(new MyPlace(p.getId() + "", ugaCAN));
			}
			dao.storePlaceConfiguration(placeList);
			
			return myplaces;
			
		}else{
			
			List<MyPlace> results = new ArrayList<MyPlace>();
			
			// skip record (-1, ugaCAN)
			for (MyPlace my : myplaces){
				if (!my.getPlaceId().equals("-1")) results.add(my);
			}
			
			return results;
			
		}
		
    }

}
