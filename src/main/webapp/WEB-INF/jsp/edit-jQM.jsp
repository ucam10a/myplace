<%--

    Licensed to Jasig under one or more contributor license
    agreements. See the NOTICE file distributed with this work
    for additional information regarding copyright ownership.
    Jasig licenses this file to you under the Apache License,
    Version 2.0 (the "License"); you may not use this file
    except in compliance with the License. You may obtain a
    copy of the License at:

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on
    an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied. See the License for the
    specific language governing permissions and limitations
    under the License.

--%>

<jsp:directive.include file="/WEB-INF/jsp/include.jsp"/>

<portlet:actionURL var="formUrl">
    <portlet:param name="action" value="editPOI"/>
</portlet:actionURL>

    <h3> &nbsp; Select your points of interest: </h3>
    <div>
        <form name="editPoiForm" action="${ formUrl }" method="POST">
        <div style="padding-left: 10px;">
            <div class="ui-grid-a">
	            <c:forEach items="${ mypois }" var="poi" varStatus="status">
	                <div class="ui-block-a" style="width:70%; height:45px; padding-top: 10px;" >
		                <font size="+1"><b>${ poi.place.name }</b></font>
                    </div><!-- /ui-block -->
                    <div class="ui-block-b" style="width:30%;" >
                        <select name="${ poi.place.id }" id="flip-${ status.index }" data-role="slider" data-mini="true">
                            <option value="off">Off</option>
                            <option value="on" <c:if test="${ poi.select }">selected=""</c:if>>On</option>
                        </select>
                    </div><!-- /ui-block -->
                </c:forEach>
            </div><!-- /ui-grid -->
            <input name="ugaCAN" type="hidden" value="${ ugaCAN }"/>
			<input id="selectedPoi" name="selectedPoi" type="hidden" value="${ selectedPoiId }"/>
		</div>
        <a href="javascript:void(null);" data-role="button" style="width:80%; margin-left: auto; margin-right: auto; text-shadow: 0 0 0 white;" onclick="get_check_value(${ fn:length(mypois) });" >Submit</a>
        </form>
        <br />
        <br />
    </div>

<script type="text/javascript">

function get_check_value()
{
    var selectedPoi = document.getElementById('selectedPoi'); // hidden input
    selectedPoi.value = "";
    for (var i = 0; i < ((arguments[0] - 1) + 1); i++){
    	var flip = document.getElementById('flip-' + i);
    	if (((arguments[0] - 1) + 1) == 1) {
            if (flip.value == "on"){
                if (selectedPoi.value == ""){
                    selectedPoi.value = selectedPoi.value + flip.name;
                }else {
                    selectedPoi.value = selectedPoi.value + ',' + flip.name;
                }
            }
        }else {
            if (flip.value == "on"){
                if (selectedPoi.value == ""){
                    selectedPoi.value = selectedPoi.value + flip.name;
                }else {
                    selectedPoi.value = selectedPoi.value + ',' + flip.name;
                }
            }
        }   
    }
    //alert(selectedPoi.value);
    document.editPoiForm.submit();
}

</script>