<%--

    Licensed to Jasig under one or more contributor license
    agreements. See the NOTICE file distributed with this work
    for additional information regarding copyright ownership.
    Jasig licenses this file to you under the Apache License,
    Version 2.0 (the "License"); you may not use this file
    except in compliance with the License. You may obtain a
    copy of the License at:

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on
    an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied. See the License for the
    specific language governing permissions and limitations
    under the License.

--%>

<jsp:directive.include file="/WEB-INF/jsp/include.jsp"/>

<portlet:renderURL var="editFormUrl" >
    <portlet:param name="action" value="editForm"/>
</portlet:renderURL>

<style type="text/css">
    
    .contentInfo {
         font-weight: bold;
         height: 20px;
         font-size: 120%;
         padding-top: 5px;
         padding-bottom: 5px;
    }
    
    .tdDiv{
         background: #D1D1CF;
         border-radius: 5px;
    }

</style>

        <c:if test="${ fn:length(mypois) == 0 }">
            <h3 style="margin-left: 15px;">No Point of Interest</h3>
        </c:if>
        <c:if test="${ fn:length(mypois) != 0 }">
            <h3 style="margin-left: 15px;">Points of Interest</h3>
        </c:if>
        <c:if test="${not empty ugaCAN}">
            <h3 style="margin-left: 15px;"><a href="${ editFormUrl }" >Customize</a></h3>
		</c:if>
        
        <div>
        <ul data-role="listview" >
            <c:forEach items="${ mypois }" var="poi" varStatus="status">
                <li id="li-${ status.index }" class="contentInfo" style="background: white; height: 40px;" onclick="displayResult(${ status.index })" >
                    <span id="a-${ status.index }" style="color: #5C5C5C; text-shadow: 0 0 0 #5C5C5C;">
                        ${ poi.place.name }
                        <br />
                        <font style="font-size: 90%;">Bldg #${ poi.place.code }</font>
                    </span>
                    <c:if test="${ poi.open == true }">
                        <span style="position:absolute; right: 50px; top:40%;"><img src="/staticmobile/MyPlaces/greenlight.png" style="width: 15px;" /></span>
                    </c:if>
                    <c:if test="${ poi.open == false }">
                        <span style="position:absolute; right: 50px; top:40%;"><img src="/staticmobile/MyPlaces/redlight.png" style="width: 15px;" /></span>
                    </c:if>
                    <span id="angleBracket-${ status.index }" style="position:absolute; top:35%; right: 20px; font-weight: bold;">&gt;</span>
                </li>
                <div id="result${ status.index }" style="display: none; width: 85%; margin: auto;" >
                    <br />
                    <c:if test="${ poi.place.img != '' }">
                        <div style="marign-left: auto; marign-right: auto;"><img src="${ poi.place.img }" style="width: 100%;" /></div>
                    </c:if>
                    <c:set var="HourAttr" value="Today's Hours"/>
                    <table style="width: 100%;">
                         <c:forEach items="${ poi.attributes }" var="attribute">
                             <c:set var="attrName" value="${ attribute.attributeName }"/>
                             <% String attrStyleWidth = "100%"; %>
                             <c:if test="${ not empty attribute.value }">
                                 <tr>
                                     <td class="contentInfo">
                                         <c:choose>
                                             <c:when test="${ attrName ne HourAttr }">
                                                 <div class="tdDiv">
                                                 <% attrStyleWidth = "30%"; %>
                                             </c:when>
                                         </c:choose>
                                         <c:if test="${ attrName ne HourAttr }"><div class="tdDiv"><% attrStyleWidth = "30%"; %></c:if>
                                         <div class="ui-grid-a" style="padding: 10px;">
                                         <div class="ui-block-a" style="width: <%=attrStyleWidth %>;"><font color="red"> ${ fn:trim(attrName) } : </font></div>
                                         <c:choose>
                                             <c:when test="${ fn:trim(attrName) == 'Email' }">
                                                 <div class="ui-block-b" style="width: 65%;" ><a href="mailto:${ attribute.value }">${ fn:escapeXml(attribute.value) }</a></div>
                                             </c:when>
                                             <c:when test="${ fn:trim(attrName) == 'Phone' }">
					                             <div class="ui-block-b" style="width: 65%;" ><a href="tel:${ attribute.value }">${ fn:escapeXml(attribute.value) }</a></div>
                                             </c:when>
                                             <c:when test="${ fn:trim(attrName) == 'address' }">
					                             <div class="ui-block-b" style="width: 65%;" >
					                                 <a href="http://maps.google.com.tw/maps?f=q&hl=en&geocode=&q=${ poi.place.latitude },${ poi.place.longitude }">${ fn:escapeXml(attribute.value) }</a>
                                                     <br /><br />
                                                     <a href="javascript:void(null);" onclick="getDirection(${ poi.place.latitude },${ poi.place.longitude });" >Directions</a>
                                                 </div>
                                             </c:when>
                                             <c:otherwise>
                                                 <div class="ui-block-b" style="width: 100%; font-size: 80%;" >${ fn:escapeXml(attribute.value) }
                                                 <br />
                                                 <c:if test="${ attrName eq HourAttr }">
                                                     *Note: Open hours may change due to special events.
                                                 </c:if>
                                                 </div>
                                             </c:otherwise>
                                         </c:choose>
                                         </div>
                                         <c:choose>
                                             <c:when test="${ attrName ne HourAttr }">
                                                 </div>
                                             </c:when>
                                         </c:choose>
                                     </td>
                                 </tr>
                             </c:if>
                         </c:forEach>
                    </table>
                    <br />
                </div>
            </c:forEach>
        </ul>
        </div>
        <br />
        <br />
        <br />
        
<script type="text/javascript">

var dest_lon = 0;
var dest_lat = 0;
var support = false;

function displayResult(index) {

    var result = document.getElementById("result" + index);
    if (result.style.display == "none"){
        result.style.display = "block";
        var li = document.getElementById("li-" + index);
        var style = li.getAttribute("style");
        style = "background: red; color: white; height: 40px;";
        li.setAttribute("style", style);
        var aTag = document.getElementById("a-" + index);
        aTag.style.color = "white"; 
        changeArrow(index);
    }else{
        result.style.display = "none";
        var li = document.getElementById("li-" + index);
        var style = li.getAttribute("style");
        style = "background: white; height: 40px;";
        li.setAttribute("style", style);
        var aTag = document.getElementById("a-" + index);
        aTag.style.color = "#5C5C5C"; 
        changeArrow(index);
    }

}

function changeArrow(index){
	
	var node = document.getElementById('angleBracket-' + index);
	var arrow = node.innerHTML;
	if (arrow == '&gt;'){
		node.innerHTML = '&#8744;';
	}else{
		node.innerHTML = '&gt;';
	}
	
}


function getPositionSuccess(position){
    
	//alert(position.coords.latitude);
    //alert(position.coords.longitude);
    
    window.location = "http://maps.google.com/?saddr=" + position.coords.latitude + "," + position.coords.longitude + "&daddr=" + dest_lat + "," + dest_lon;

}

function getPositionError(error){
    switch (error.code) {
	    case error.TIMEOUT:
		    alert("time out");
		    break;
	    case error.PERMISSION_DENIED:
		    alert("reject to get position !");
		    break;
	    case error.POSITION_UNAVAILABLE:
		    alert("unavailable");
		    break;
    }
}

function getDirection(lat, lon){
	
	dest_lon = lon;
	dest_lat = lat;
	
	if (support){
		navigator.geolocation.getCurrentPosition(getPositionSuccess, getPositionError);
	}else{
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(getPositionSuccess, getPositionError);
			support = true;
		}
		else {
			alert("browser not support");
		}
	}
	
}

function ReplaceAll(strOrg, strFind, strReplace){
	var index = 0;
	while(strOrg.indexOf(strFind,index) != -1){
		strOrg = strOrg.replace(strFind,strReplace);
		index = strOrg.indexOf(strFind,index);
	}
	return strOrg;	
}

up.jQuery(function() {
    var $ = up.jQuery;
    var fluid = up.fluid;
    var login = "${not empty ugaCAN}"; 

    $(document).ready(function() { 

    	// change the background color
	    //var style = document.getElementById("page").getAttribute("style");
	    //style = style + "background: linear-gradient(#DC0000, #B40000) repeat scroll 0 0 #C80000;";
	    //document.getElementById("page").setAttribute("style", style);
	    if (login == "false"){
	    	alert("To customize your list of places, click the gear icon on the home screen to log in");
	    }
	    
	    
    });
});

</script>
