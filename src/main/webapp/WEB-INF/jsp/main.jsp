<%--

    Licensed to Jasig under one or more contributor license
    agreements. See the NOTICE file distributed with this work
    for additional information regarding copyright ownership.
    Jasig licenses this file to you under the Apache License,
    Version 2.0 (the "License"); you may not use this file
    except in compliance with the License. You may obtain a
    copy of the License at:

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on
    an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied. See the License for the
    specific language governing permissions and limitations
    under the License.

--%>

<jsp:directive.include file="/WEB-INF/jsp/include.jsp"/>

<portlet:renderURL var="editFormUrl" >
    <portlet:param name="action" value="editForm"/>
</portlet:renderURL>

<c:if test="${ fn:length(mypois) == 0 }">
    <font size="+0.5"><b> &nbsp;  &nbsp; No POIs </b></font>
</c:if>
<c:if test="${ fn:length(mypois) != 0 }">
    <font size="+0.5"><b> &nbsp;  &nbsp; Your POIs </b></font>
</c:if>
<c:if test="${not empty ugaCAN}">
    <br /><a href="${ editFormUrl }" ><b>Customize</b></a>
</c:if>
<div>
    <ul>
        <c:forEach items="${ mypois }" var="poi" varStatus="status">
            <li id="li-${ status.index }" style="background: white;" >
                <a id="a-${ status.index }" href="javascript:void(null);" onclick="displayResult_myplace(${ status.index })" style="color: #5C5C5C; text-shadow: 0 0 0 white;">
                    <font size="+0.5">${ poi.place.name } Bldg#${ poi.place.code }</font>
                </a>
                &nbsp;  &nbsp; 
                <c:if test="${ poi.open == true }">
                    <img src="${environment.build.uportal.protocol}://${environment.build.uportal.server}/staticmobile/MyPlaces/greenlight.png" style="width: 15px;" />
                </c:if>
                <c:if test="${ poi.open == false }">
                    <img src="${environment.build.uportal.protocol}://${environment.build.uportal.server}/staticmobile/MyPlaces/redlight.png" style="width: 15px;" />
                </c:if>
            </li>
            <div id="result_myplace${ status.index }" style="display: none; width: 100%; margin: auto;" >
                <br />
                <c:if test="${ poi.place.img != '' }">
                    <div style="marign-left: auto; marign-right: auto;  width: 400px;"><img src="${ poi.place.img }" style="width: 100%;" /></div>
                </c:if>
                <table style="width: 100%;">
                    <c:forEach items="${ poi.attributes }" var="attribute">
                        <c:set var="attrName" value="${ attribute.attributeName }"/>
                        <% String attrStyleWidth = "100%"; %>
                        <c:if test="${ not empty attribute.value }">
                            <tr>
                                <td class="contentInfo">
                                    <c:choose>
                                        <c:when test="${ attrName != 'Today Hours' }">
                                             <% attrStyleWidth = "30%"; %>
                                        </c:when>
                                    </c:choose>
                                    <c:if test="${ attrName != 'Today Hours' }"><div class="tdDiv"><% attrStyleWidth = "30%"; %></c:if>
                                        <div class="ui-grid-a" style="padding: 10px;">
                                        <div class="ui-block-a" style="width: <%=attrStyleWidth %>;"><font color="red"> ${ fn:trim(attrName) } : </font></div>
                                        <c:choose>
                                            <c:when test="${ fn:trim(attrName) == 'Email' }">
                                                <div class="ui-block-b" style="width: 65%;" ><a href="mailto:${ attribute.value }">${ fn:escapeXml(attribute.value) }</a></div>
                                            </c:when>
                                            <c:when test="${ fn:trim(attrName) == 'Phone' }">
					                            <div class="ui-block-b" style="width: 65%;" ><a href="tel:${ attribute.value }">${ fn:escapeXml(attribute.value) }</a></div>
                                            </c:when>
                                            <c:when test="${ fn:trim(attrName) == 'address' }">
					                            <div class="ui-block-b" style="width: 65%;" >
					                                <a href="http://maps.google.com.tw/maps?f=q&hl=en&geocode=&q=${ poi.place.latitude },${ poi.place.longitude }">${ fn:escapeXml(attribute.value) }</a>
                                                    <br /><br />
                                                    <a href="javascript:void(null);" onclick="getDirection(${ poi.place.latitude },${ poi.place.longitude });" >Directions</a>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="ui-block-b" style="width: 100%; font-size: 80%;" >${ fn:escapeXml(attribute.value) }
                                                <br />
                                                <c:if test="${ attrName == 'Today Hours' }">
                                                    *Note: Open hours may change due to special events.
                                                </c:if>
                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                        </div>
                                     </td>
                                 </tr>
                             </c:if>
                         </c:forEach>
                    </table>
                    <br />
                </div>
                <br />
            </c:forEach>
        </ul>
    </div>
    
<script type="text/javascript">

var dest_lon = 0;
var dest_lat = 0;
var support = false;

function displayResult_myplace(index) {

    var result = document.getElementById("result_myplace" + index);
    if (result.style.display == "none"){
        result.style.display = "block";
    }else{
        result.style.display = "none";
    }

}

function getPositionSuccess(position){
    
	//alert(position.coords.latitude);
    //alert(position.coords.longitude);
    
    window.location = "http://maps.google.com/?saddr=" + position.coords.latitude + "," + position.coords.longitude + "&daddr=" + dest_lat + "," + dest_lon;

}

function getPositionError(error){
    switch (error.code) {
	    case error.TIMEOUT:
		    alert("time out");
		    break;
	    case error.PERMISSION_DENIED:
		    alert("reject to get position !");
		    break;
	    case error.POSITION_UNAVAILABLE:
		    alert("unavailable");
		    break;
    }
}

function getDirection(lat, lon){
	
	dest_lon = lon;
	dest_lat = lat;
	
	if (support){
		navigator.geolocation.getCurrentPosition(getPositionSuccess, getPositionError);
	}else{
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(getPositionSuccess, getPositionError);
			support = true;
		}
		else {
			alert("browser not support");
		}
	}
	
}

function ReplaceAll(strOrg, strFind, strReplace){
	var index = 0;
	while(strOrg.indexOf(strFind,index) != -1){
		strOrg = strOrg.replace(strFind,strReplace);
		index = strOrg.indexOf(strFind,index);
	}
	return strOrg;	
}

up.jQuery(function() {
    var $ = up.jQuery;
    var fluid = up.fluid;
    var login = "${not empty ugaCAN}"; 
    
    $(document).ready(function() { 
    	if (login == "false"){
	    	alert("To customize your list of places, click the gear icon on the home screen to log in");
	    }
    });
});

</script>
